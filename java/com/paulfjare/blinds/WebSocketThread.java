package com.paulfjare.blinds;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;

import com.neovisionaries.ws.client.PayloadGenerator;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketExtension;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by pfjar on 4/17/2016.
 */
public class WebSocketThread extends HandlerThread {


    private static final int TIMEOUT = 5000;

    private static final String TAG = WebSocketThread.class.getSimpleName();

    private String mDeviceAddress;

    private Handler mHandler;
    private Handler mResponseHandler;

    private Callback mCallback;

    protected WebSocket mWebSocket;
    private Runnable mRunnable;

    public interface Callback {
       void onStringReturned(String s);
            void onWebSocketDisconnect();
    }

    public WebSocketThread(Handler responseHandler, Callback callback, String s) throws IOException, WebSocketException {
        super(TAG);
        mResponseHandler = responseHandler;
        mCallback = callback;
        mDeviceAddress = s;
    }

    @Override
    public void run() {
        super.run();
    }

    public void queueTask(String s){
        Log.d(TAG, "Added to queue");
        mHandler.obtainMessage(0, s).sendToTarget();
    }

    public void prepareHandler() throws IOException {
        mHandler = new Handler(getLooper(), new Handler.Callback(){
            @Override
            public boolean handleMessage(Message msg) {


                String s =(String) msg.obj;
                handleRequest(s);

                return true;
            }
        });
        mRunnable = new Runnable(){

            public  void run(){
                try {
                    mWebSocket =  connect();

                } catch (WebSocketException | IOException e) {
                    e.printStackTrace();
                    mWebSocket=null;
                    mResponseHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mCallback.onStringReturned("WebSocket could not connect.");
                            mCallback.onStringReturned("Retrying ...");
                        }
                    });
//                    mHandler.postDelayed(mRunnable, 5*1000);
                }

            }
        };
        mHandler.postDelayed(mRunnable, 1000);


    }
    public void disconnectWebSocket(){
        Runnable runnable = new Runnable(){

            public  void run(){
                if(mWebSocket!=null) {
                    mWebSocket.disconnect();
                }
            }
        };
        mHandler.postDelayed(runnable, 1000);
    }

    private void handleRequest(String s){
        mWebSocket.sendText(s);
        Log.d(TAG, "Sending Text");
    }

    public  WebSocket connect() throws IOException, WebSocketException
    {
        return new WebSocketFactory()
                .setConnectionTimeout(TIMEOUT)
                .createSocket(mDeviceAddress)
                .addExtension(WebSocketExtension.PERMESSAGE_DEFLATE)
                .addListener(new WebSocketAdapter() {

                    public void onConnected(WebSocket websocket,Map<String, List<String>> headers){
                        Log.d(TAG, "On Connected");

                        mResponseHandler.post(new Runnable() {
                            @Override
                            public void run() {

                                mCallback.onStringReturned("WebSocket connected");

                            }
                        });

                    }

                    public void onTextMessage(WebSocket websocket, final String message) {
                        Log.d(TAG, "Text Listener:" + message);
                        System.out.println(message);
                    }
                    public void onFrameError(WebSocket websocket, WebSocketException cause, WebSocketFrame frame){
                        Log.d(TAG, "WebSocket error");
                        mWebSocket.disconnect();
                    }

                    public void onDisconnected(WebSocket websocket,
                                               WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame,
                                               boolean closedByServer){
                        Log.d(TAG, "WebSocket disconnected");
                        mResponseHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mCallback.onWebSocketDisconnect();
                                mCallback.onStringReturned("WebSocket disconnected.");
                            }
                        });

                    }

                }).setPongInterval(10 * 1000).setPongPayloadGenerator(new PayloadGenerator() {
            @Override
            public byte[] generate() {
                // The string representation of the current date.
                return new Date().toString().getBytes();
            }
        })
                .connect();
    }
    private static BufferedReader getInput() throws IOException
    {
        return new BufferedReader(new InputStreamReader(System.in));
    }

}
