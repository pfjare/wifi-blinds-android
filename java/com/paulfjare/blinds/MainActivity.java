package com.paulfjare.blinds;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;

import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketExtension;
import com.neovisionaries.ws.client.WebSocketFactory;


import org.apache.commons.validator.routines.UrlValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.paulfjare.blinds.R.xml.preferences;


public class MainActivity extends AppCompatActivity implements WebSocketThread.Callback{
    private static  final String TAG = "Blinds";



    /*
    Blinds states
    */

    // No light
    private static final int CLOSED = 0;
    // Half open with blades angled downward
    private static final int OPEN_HALF = 1;
    // Maximum light
    private static final int FULL = 2;
    // Half open with blades angled upward
    private static final int OPEN_HALF_FLIP = 3;

    Context mContext;
    WebSocketThread mWebSocketThread;
    CoordinatorLayout mCoordinatorLayout;
    String mModuleAddress;
    FloatingActionButton mFab0;
    FloatingActionButton mFab1;
    FloatingActionButton mFab2;
    FloatingActionButton mFab3;
    SharedPreferences sharedPref;
    Snackbar mSnackbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mContext = getApplicationContext();

        PreferenceManager.setDefaultValues(this, preferences, false);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);

        mSnackbar = Snackbar .make(mCoordinatorLayout,"Default Snackbar message",Snackbar.LENGTH_LONG);


        mFab0 = (FloatingActionButton) findViewById(R.id.fab0);
        mFab0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeAction(0);
            }
        });
        mFab1 = (FloatingActionButton) findViewById(R.id.fab1);
        mFab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeAction(1);
            }
        });
        mFab2 = (FloatingActionButton) findViewById(R.id.fab2);
        mFab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeAction(2);
            }
        });
        mFab3 = (FloatingActionButton) findViewById(R.id.fab3);
        mFab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takeAction(3);
            }
        });


    }

    @Override
    public void onStringReturned(String s){

        mSnackbar.setText(s).show();

    }
    @Override
    public void onWebSocketDisconnect(){
        if (mWebSocketThread != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mWebSocketThread.quitSafely();
            } else {
                mWebSocketThread.quit();
            }
            mWebSocketThread = null;
        }
    }

    @Override
    protected void onResume(){
        super.onResume();

        initWebSocket();

    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mWebSocketThread != null){
            mWebSocketThread.disconnectWebSocket();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initWebSocket(){
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        mModuleAddress = "ws://" + sharedPref.getString("pref_module_address", "default value");

        boolean validURI;

        String[] customSchemes = {"ws"};
        UrlValidator customValidator = new UrlValidator(customSchemes);
        if (!customValidator.isValid(mModuleAddress)) {
            validURI = false;
        }else {
            validURI = true;
        }

        if(validURI) {
            try {
                mWebSocketThread = new WebSocketThread(new Handler(), this, mModuleAddress);
            } catch (IOException | WebSocketException e) {
                e.printStackTrace();
            }
            mWebSocketThread.start();
            try {
                mWebSocketThread.prepareHandler();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            mSnackbar.setText("The device IP is invalid").show();
        }

    }
    public void takeAction(int state){
        switch (state){
            case CLOSED:
                mSnackbar.setText("Closed Down").show();
                mWebSocketThread.queueTask("#-72");
                break;
            case OPEN_HALF:
                mSnackbar.setText("Part Open").show();
                mWebSocketThread.queueTask("#-50");
                break;
            case FULL:
                mSnackbar.setText("Open").show();
                mWebSocketThread.queueTask("#-20");
                break;
            case OPEN_HALF_FLIP:
                mSnackbar.setText("Closed Up").show();
                mWebSocketThread.queueTask("#60");
                break;
        }
    }

}
